# Cargem

Cargem allows the user instantiate conveniently configured Car objects in various versions.

Completed as a technical exercise during interview process for Best Buy.

## Installation

Install this gem directly with:

`$ gem install compensate`

## Usage

### Command-line

This gem includes a packaged bin/ executable that demonstrates behavior, for convenience.

After installation, call `cargem-drive-camaro` at the command line to see a Camaro in action!

### Ruby script

Here's an example use, where we generate a Camaro-type Car object and tell it to drive.

After the gem is installed, initialize a script for our code to live in:

`$ touch main.rb`

Edit this script to include the following code snippet:

```ruby
#!/usr/bin/env ruby

require 'cargem'

car = Cargem::Camaro.new
car.drive
```

Save the file, and change the permissions to make it executable:

`$ chmod +x main.rb`

Then run the script:

`$ ./main.rb`

You should see output of the car accelerating up to its maximum speed.

### Class methods

* `Car#accelerate  # Increments current_speed by 1`
* `Car#drive # Drive calls accelerate until it hits max speed`

## Development

After checking out the repo, run `bin/setup` to install dependencies. Then, run `rake spec` to run the tests. You can also run `bin/console` for an interactive prompt that will allow you to experiment.

To install this gem onto your local machine, run `bundle exec rake install`. To release a new version, update the version number in `version.rb`, and then run `bundle exec rake release`, which will create a git tag for the version, push git commits and tags, and push the `.gem` file to [rubygems.org](https://rubygems.org).
