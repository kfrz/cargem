require "spec_helper"

describe Cargem::Camaro do
  it "creates a new Camaro with the correct properties" do
    c = Cargem::Camaro.new
    expect(c.brand).to eq("Chevy")
    expect(c.max_speed).to eq(200)
  end
end
