require "spec_helper"

describe Cargem::Car do
  let(:car) { Cargem::Car.new }

  it "creates a new Car" do
    expect(car.brand).to eq("unknown")
  end

  it "should respond to #accelerate" do
    expect { car.accelerate }.to change(car, :current_speed).from(0).to(1)
  end

  it "should respond to #drive" do
    expect { car.drive }.to change(car, :current_speed).by(1)
  end
end
