module Cargem
  class Car
    attr_accessor :brand, :current_speed, :max_speed

    def initialize
      @brand = brand ||'unknown'
      @current_speed = current_speed || 0
      @max_speed = max_speed || 0
    end

    def drive
      while @current_speed <= @max_speed
        puts "#{@current_speed}"
        accelerate
      end
      print stopped_acceleration_message
    end

    def accelerate
      @current_speed += 1
    end

    private

    def stopped_acceleration_message
      "The #{brand} has hit its max speed of #{@max_speed} and will now stop accelerating."
    end
  end
end
