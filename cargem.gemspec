
lib = File.expand_path('lib', __dir__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'cargem/version'

Gem::Specification.new do |spec|
  spec.name          = 'cargem'
  spec.version       = Cargem::VERSION
  spec.authors       = ['Keifer Furzland']
  spec.email         = ['kfrz.code@gmail.com']

  spec.summary       = %q('This is a simple car-generation gem')
  spec.description   = %q('This is meant to be an example of how to build a simple Ruby gem.')
  spec.homepage      = 'https://gitlab.com/kfrz/cargem'

  spec.files         = Dir.chdir(File.expand_path(__dir__)) do
    `git ls-files -z`.split("\x0").reject { |f| f.match(%r{^(test|spec|features)/}) }
  end

  spec.executables << 'cargem-drive-camaro'
  spec.require_paths = ['lib']

  spec.add_development_dependency 'bundler', '~> 1.16'
  spec.add_development_dependency 'rake', '~> 10.0'
  spec.add_development_dependency 'rspec', '~> 3.0'
end
